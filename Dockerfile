
FROM debian:jessie

# Install build requeriments the compile php , ds, posix pthread extension
# the do some cleanup
RUN echo "America/Sao_Paulo" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

RUN apt-get update \
	&& apt-get upgrade -y \
	&& apt-get install -y autoconf  build-essential libxml2-dev libev.dev  libssl-dev libgdbm-dev libgdbm3 curl libcurl4-gnutls-dev  wget \
	&& wget -qO php-7.2.3.tar.bz2 http://br2.php.net/get/php-7.2.3.tar.bz2/from/this/mirror \
	&& tar xjf php-7.2.3.tar.bz2 \
	&& cd php-7.2.3 \
	&& ./configure \
		--disable-cgi \
		--enable-mbstring \		
		--enable-zip \
		--with-libdir=/lib/x86_64-linux-gnu \		
		--with-curl \		
		--with-openssl \		
	&& make \
	&& make install \
	&& cp php.ini-production /usr/local/lib/php.ini \
	&& sed -i "s/^;date.timezone =$/date.timezone = \"UTC\"/" /usr/local/lib/php.ini \
	&& cd .. \
	&& pecl config-set php_ini /usr/local/lib/php.ini \
	&& pear config-set php_ini /usr/local/lib/php.ini \	
	&& pecl install ds \
	&& rm php-7.2.3.tar.bz2 \
	&& rm -rf php-7.2.3 \
	&& apt-get purge -y autoconf build-essential wget .+-dev \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

	RUN curl -sS https://getcomposer.org/installer | \
    php -- --install-dir=/usr/bin/ --filename=composer



<?php

namespace Sistema\Livraria\Tests;
use PHPUnit\Framework\TestCase;
use Sistema\Livraria\Livro\Livro;


class LivroTest extends TestCase
{
   
    public function testTitulos()
    {
        $livro = new Livro("Tunico e Tinoco na terra do Harry", '23.22');
        $this->assertNotEmpty($livro->titulos());
        
    }
}

<?php

namespace Sistema\Livraria\Tests;
use PHPUnit\Framework\TestCase;
use Sistema\Livraria\Carrinho\Carrinho;

class CarrinhoTest extends TestCase
{
    public function testCarrinhoUmLivro()
    {
        $carrinho = new Carrinho();
        $this->assertEquals("1 copia do Livro Harry Potter  custa  42.00 reais " , $carrinho->adicionar(1, 'Livro Harry Potter ', 0, '42.00'));
    }
   
    public function testCarrinhoDoisLivros()
    {
        $carrinho = new Carrinho();
        $this->assertEquals("2 copias do Livro Harry Potter  custa  79.80 reais " , $carrinho->adicionar(2, 'Livro Harry Potter ', 5, '42.00'));
   
    }
    public function testCarrinhoTresLivros()
    {
        $carrinho = new Carrinho();
        $this->assertEquals("3 copias do Livro Harry Potter  custa  113.40 reais " , $carrinho->adicionar(3, 'Livro Harry Potter ', 10, '42.00'));

    }
    public function testCarrinhoQuatroLivros()
    {
        $carrinho = new Carrinho();
        $this->assertEquals("4 copias do Livro Harry Potter  custa  142.80 reais " , $carrinho->adicionar(4, 'Livro Harry Potter ', 15, '42.00'));

    }
    public function testCarrinhoCincoLivros()
    {
        $carrinho = new Carrinho();
        $this->assertEquals("5 copias do Livro Harry Potter  custa  168.00 reais " , $carrinho->adicionar(5, 'Livro Harry Potter ', 20, '42.00'));

    }
    public function testCarrinhoSeisLivros()
    {
        $carrinho = new Carrinho();
        $this->assertEquals("6 copias do Livro Harry Potter  custa  201.60 reais " , $carrinho->adicionar(6, 'Livro Harry Potter ', 20, '42.00'));

    }
    public function testCarrinhoSeteLivros()
    {
        $carrinho = new Carrinho();
        $this->assertEquals("7 copias do Livro Harry Potter  custa  235.20 reais " , $carrinho->adicionar(7, 'Livro Harry Potter ', 20, '42.00'));

    }
    public function testCarrinhoOitoLivros()
    {
        $carrinho = new Carrinho();
        $this->assertEquals("8 copias do Livro Harry Potter  custa  268.80 reais " , $carrinho->adicionar(8, 'Livro Harry Potter ', 20, '42.00'));

    }
    
}

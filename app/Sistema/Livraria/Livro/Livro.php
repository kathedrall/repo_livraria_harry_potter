<?php

namespace Sistema\Livraria\Livro;

class Livro 
{
    private $livro;
    private $valor;
  
    public function __construct(string $livro,
        float $valor
    )
    {
        $this->livro = $livro;
        $this->valor = $valor;
        
    }
    
    public function titulos() : string
    {
        $titulo = 'O titulo do livro é '. $this->livro . ' e o valor é: ' . $this->valor;
        return $titulo;
        
    }
    
  
}

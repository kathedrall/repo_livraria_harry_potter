<?php

namespace Sistema\Livraria\Carrinho;

class Carrinho
{
    private $total;
    private $frase;
    
    public function adicionar(int $copia, 
        string $livro, 
        int $desconto, 
        float $valor
    ) : string
    {
        $this->total = self::calcular($copia, $desconto,$valor);
        $this->frase = $copia . $this->numeroCopias($copia) . 'do '. $livro . ' custa  ' . self::formatoValor($this->total) . ' reais ';
        
        return $this->frase;
    }   
    private function calcular(int $copia, 
        int $desconto, 
        float $valor
    ) : float
    {
        $valor = ($copia * $valor);
        return ($valor - ($valor * $desconto) / 100) ;
   
    }
    private static function numeroCopias(int $quantidade)
    {
        return ($quantidade <= 1)? " copia " : " copias ";
    }
    
    private static function  formatoValor(float $valor)
    {
       return number_format($valor, 2, '.', ',');
    }
   
 
}